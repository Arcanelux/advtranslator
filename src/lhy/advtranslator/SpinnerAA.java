package lhy.advtranslator;

import java.util.ArrayList;

import lhy.advtranslator.fixvalue.TranslateCode;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SpinnerAA extends ArrayAdapter<TranslateCode>{
	private final String TAG = this.getClass().getSimpleName();
	private Context mContext;
	private ArrayList<TranslateCode> codeList;
	
	public SpinnerAA(Context context, int textViewResourceId, ArrayList<TranslateCode> codeList) {
		super(context, textViewResourceId, codeList);
		this.mContext = context;
		this.codeList = codeList;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TranslateCode curCode = codeList.get(position);
		if(convertView==null){
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView= inflater.inflate(R.layout.spinner_item, null);
		}
		TextView tv = (TextView) convertView.findViewById(R.id.tvSpItem);
		tv.setText(curCode.getName());
		
		return convertView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TranslateCode curCode = codeList.get(position);
		if(convertView==null){
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView= inflater.inflate(R.layout.spinner_view, null);
		}
		TextView tv = (TextView) convertView.findViewById(R.id.tvSpItem);
		tv.setText(curCode.getName());
		
		return convertView;
	}
	
	
	
	
}
