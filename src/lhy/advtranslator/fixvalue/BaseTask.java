package lhy.advtranslator.fixvalue;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class BaseTask extends AsyncTask<Void, Void, Void>{
	private final String TAG = this.getClass().getSimpleName();
	private Context mContext;
	
	private ProgressDialog mProgressDialog;

	public BaseTask(Context mContext) {
		this.mContext = mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.show();
	}
	@Override
	protected Void doInBackground(Void... params) {
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		mProgressDialog.dismiss();
	}
	
	

	

}
