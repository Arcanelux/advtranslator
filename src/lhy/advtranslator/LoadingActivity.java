package lhy.advtranslator;

import lhy.advtranslator.function.C;
import lhy.advtranslator.function.Lhy_Network.Lhy_HttpClient;
import lhy.advtranslator.function.Pref;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.ActionBar;

public class LoadingActivity extends Activity {
	private final String TAG = this.getClass().getSimpleName();
	private Context mContext;

	private LinearLayout llLoadingProgress;
	private ProgressBar mProgressBar;
	private ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout .loading);
		mContext = this;

//		mActionBar = getSupportActionBar();
//		mActionBar.hide();

		llLoadingProgress = (LinearLayout) findViewById(R.id.llLoadingProgress);
		new InitializeTask().execute();
	}

	private class InitializeTask extends AsyncTask<Void, Void, Void>{
		String resultJsonString = "";
		JSONObject jsonObjectAndroid;
		Lhy_HttpClient jsonClient = new Lhy_HttpClient();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressBar = new ProgressBar(mContext);
			llLoadingProgress.setGravity(Gravity.CENTER_HORIZONTAL);
			llLoadingProgress.addView(mProgressBar, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		}

		@Override
		protected Void doInBackground(Void... params) {
			jsonClient.setUrl("http://static.3rddev.org/header-android.json");
			jsonClient.setMethod("get");
			jsonClient.execute();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			resultJsonString = jsonClient.getResult();
			try {
				jsonObjectAndroid = new JSONObject(resultJsonString);
				Pref.setHeaderJsonObject(mContext, jsonObjectAndroid);
				mProgressBar.setVisibility(View.GONE);
				Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			} catch(Exception e){
				e.printStackTrace();
			}

		}
	}

}
