package lhy.advtranslator;

import java.util.Calendar;
import java.util.Iterator;

import lhy.advtranslator.data.DataSentence;
import lhy.advtranslator.fixvalue.TranslateCode;
import lhy.advtranslator.function.C;
import lhy.advtranslator.function.Lhy_Network.Lhy_HttpClient;
import lhy.advtranslator.function.Pref;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class TranslateTask extends AsyncTask<Void, Void, Void>{
	private final String TAG = this.getClass().getSimpleName();
	private Context mContext;
	
	private ProgressDialog mProgressDialog;
	private Lhy_HttpClient mHttpClient, mHttpClient2, mHttpClientJson;
	private JSONObject jObjHeader;
	
	private TextView tvOri;
	private TextView tvTranslate1, tvTranslate2;
	private EditText etInput;
	
	private TranslateCode fromLangCode;
	private TranslateCode toLangCode;
	private String translateMethodCode;
	private String translateString;

	public TranslateTask(TranslateCode curOriCode, TranslateCode curTransCode, String method, String translateString, TextView tvOri, TextView tvTranslate1, TextView tvTranslate2, EditText etInput, Context mContext){
		this.fromLangCode = curOriCode;
		this.toLangCode = curTransCode;
		this.translateMethodCode = method;
		this.translateString = translateString;
		this.tvOri = tvOri;
		this.tvTranslate1 = tvTranslate1;
		this.tvTranslate2 = tvTranslate2;
		this.etInput = etInput;
		this.jObjHeader = Pref.getHeaderJsonObject(mContext);
		this.mContext = mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		mHttpClient = new Lhy_HttpClient();
		mHttpClient.setUrl("http://translator.suminb.com/translate");
		mHttpClient.addStringData("sl", fromLangCode.getCode());
		mHttpClient.addStringData("tl", toLangCode.getCode());
//		mHttpClient.addStringData("m", translateMethodCode);
		mHttpClient.addStringData("m", C.METHOD_ORI);
		mHttpClient.addStringData("t", translateString);
		mHttpClient.setMethod("post");
		mHttpClient.setEncoding("utf-8");

		/** Set Header **/
		Iterator iter = jObjHeader.keys();
		while(iter.hasNext()){
			try {
				String key = (String) iter.next();
				String value = jObjHeader.getString(key);
				if(key.equals("Accept-Encoding")){
				} else{
					mHttpClient.addHeader(key, value);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		mHttpClient.execute();
		
		mHttpClient2 = new Lhy_HttpClient();
		mHttpClient2.setUrl("http://translator.suminb.com/translate");
		mHttpClient2.addStringData("sl", fromLangCode.getCode());
		mHttpClient2.addStringData("tl", toLangCode.getCode());
//		mHttpClient2.addStringData("m", translateMethodCode);
		mHttpClient2.addStringData("m", C.METHOD_JA);
		mHttpClient2.addStringData("t", translateString);
		mHttpClient2.setMethod("post");
		mHttpClient2.setEncoding("utf-8");

		/** Set Header **/
		Iterator iter2 = jObjHeader.keys();
		while(iter2.hasNext()){
			try {
				String key = (String) iter2.next();
				String value = jObjHeader.getString(key);
				if(key.equals("Accept-Encoding")){
				} else{
					mHttpClient2.addHeader(key, value);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		mHttpClient2.execute();
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		String resultString1 = mHttpClient.getResult();
		String resultString2 = mHttpClient2.getResult();
//		Log.d(TAG, resultString1);
		tvOri.setText(translateString);
		tvTranslate1.setText(resultString1);
		tvTranslate2.setText(resultString2);
//		etInput.setText("");
//		Calendar mCalendar = Calendar.getInstance();
//		Pref.addSentence(mContext, new DataSentence(translateString, fromLangCode, toLangCode, mCalendar));
		mProgressDialog.dismiss();
	}
}
