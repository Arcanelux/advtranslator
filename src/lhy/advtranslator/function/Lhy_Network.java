package lhy.advtranslator.function;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.content.Entity;
import android.util.Log;

public class Lhy_Network {
	private static final String TAG = "Lhy_Network";

	
	
	public static class Lhy_HttpClient{
		
		private String url;
		private String method = "get";
		private ArrayList<NameValuePair> headerList = new ArrayList<NameValuePair>();
		private ArrayList<NameValuePair> stringDataList = new ArrayList<NameValuePair>();
		private String encoding = "UTF-8";
		private String result;
		
		public void execute(){
			InputStream is = null;
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpUriRequest httpMethod = null;
				if(method.equals("post") || method.equals("POST")){
					httpMethod = new HttpPost(url);
				} else if(method.equals("get") || method.equals("GET")){
					httpMethod = new HttpGet(url);
				}

				for(int i=0; i<headerList.size(); i++){
					String name = headerList.get(i).getName();
					String value = headerList.get(i).getValue();
					httpMethod.addHeader(name, value);
					Log.d(TAG, name + " : " + value);
				}
				
				/** Post 형식일 경우 StringData 추가 **/
				UrlEncodedFormEntity ent = new UrlEncodedFormEntity(stringDataList, encoding);
				if(method.equals("post") || method.equals("POST")){
					((HttpPost) httpMethod).setEntity(ent);	
//					((HttpPost) httpMethod).set
				}
				
				HttpResponse response = httpclient.execute(httpMethod);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			}catch(Exception e){
				e.printStackTrace();
			}

			//convert response to string
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, encoding));
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result=sb.toString();
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		public String getUrl() {
			return url;
		}
		public String getMethod() {
			return method;
		}
		public String getEncoding() {
			return encoding;
		}
		public String getResult() {
			return result;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public void setMethod(String method) {
			this.method = method;
		}
		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}
		public ArrayList<NameValuePair> getHeaderList() {
			return headerList;
		}
		public void setHeaderList(ArrayList<NameValuePair> headerList) {
			this.headerList = headerList;
		}
		/**
		 * Accept : application/json
		 * Content-type : application/json
		 */
		public void addHeaderListJson(){
			this.headerList.add(new BasicNameValuePair("Accept", "application/json"));
			this.headerList.add(new BasicNameValuePair("Content-type", "application/json"));
		}
		public void addHeader(String name, String value){
			this.headerList.add(new BasicNameValuePair(name, value));
		}

		public ArrayList<NameValuePair> getStringDataList() {
			return stringDataList;
		}
		public void setStringDataList(ArrayList<NameValuePair> stringDataList) {
			this.stringDataList = stringDataList;
		}
		public void addStringData(String name, String value){
			this.stringDataList.add(new BasicNameValuePair(name, value));
		}
	}
}
