package lhy.advtranslator.function;

import java.io.IOException;
import java.util.ArrayList;

import lhy.advtranslator.data.DataSentence;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Pref {
	private static final String TAG = "AdvTranslator_Pref";

	// Density 저장
	public static void setDensity(Context context){
		SharedPreferences pref = context.getSharedPreferences("savedValue", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		float density  = context.getResources().getDisplayMetrics().density;
		editor.putFloat("density", density);
		editor.commit();
	}

	// Density 불러오기
	public static float getDensity(Context context){
		SharedPreferences pref = context.getSharedPreferences("savedValue", Context.MODE_PRIVATE);
		float density = pref.getFloat("density", 0.0f);
		return density;
	}

	// JsonObject String 형태로 저장
	public static void setJsonObject(Context context, JSONObject jsonObject){
		SharedPreferences pref = context.getSharedPreferences("savedValue", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		String strJsonObject = jsonObject.toString();
		editor.putString("jsonObject", strJsonObject);
		editor.commit();
	}

	// String 형태로 저장되어있는 JsonObject를 JsonObject객체로 생성하여 리턴
	public static JSONObject getJsonObject(Context context){
		SharedPreferences pref = context.getSharedPreferences("savedValue", Context.MODE_PRIVATE);
		String strJsonObject = pref.getString("jsonObject", "");
		try {
			return new JSONObject(strJsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	// JsonObject String 형태로 저장
	public static void setHeaderJsonObject(Context context, JSONObject jsonObject){
		SharedPreferences pref = context.getSharedPreferences("setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		String strJsonObject = jsonObject.toString();
		editor.putString("headerJsonObject", strJsonObject);
		editor.commit();
	}

	// String 형태로 저장되어있는 JsonObject를 JsonObject객체로 생성하여 리턴
	public static JSONObject getHeaderJsonObject(Context context){
		SharedPreferences pref = context.getSharedPreferences("setting", Context.MODE_PRIVATE);
		String strJsonObject = pref.getString("headerJsonObject", "");
		try {
			return new JSONObject(strJsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static void addSentence(Context context, DataSentence dataSentence){
		ArrayList<DataSentence> dataSentences = getSentences(context);
		if(dataSentences==null) dataSentences = new ArrayList<DataSentence>();
		dataSentences.add(dataSentence);
		
		for(DataSentence curDataSentence : dataSentences){
			Log.d(TAG, curDataSentence.getSentence());
		}
		
		SharedPreferences pref = context.getSharedPreferences("sentence", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		try {
			editor.putString("sentence", ObjectSerializer.serialize(dataSentences));
		} catch (IOException e) {
			e.printStackTrace();
		}
		editor.commit();
	}
	
	public static ArrayList<DataSentence> getSentences(Context context){
		SharedPreferences pref = context.getSharedPreferences("sentence", Context.MODE_PRIVATE);
		int size = pref.getInt("sentence_size", 0);
		
		ArrayList<DataSentence> dataSentences = null;
		try {
			dataSentences = ((ArrayList<DataSentence>) ObjectSerializer.deserialize(pref.getString("sentence", "")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return dataSentences;
	}
	
	
}
