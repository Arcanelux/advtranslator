package lhy.advtranslator.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBSentenceHelper extends SQLiteOpenHelper{
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "sentencesManager";

    // Contacts table name
    private static final String TABLE_CONTACTS = "sentences";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_SENTENCE = "sentence";
    private static final String KEY_TR_FROM_CODE = "tr_from_code";
    private static final String KEY_TR_FROM_NAME = "tr_from_name";
    private static final String KEY_TR_TO_CODE = "tr_to_code";
    private static final String KEY_TR_TO_NAME = "tr_to_name";
    private static final String KEY_DATETIME = "datetime";

	
	public DBSentenceHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
