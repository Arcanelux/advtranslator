package lhy.advtranslator.data;

import java.io.Serializable;
import java.util.Calendar;

import lhy.advtranslator.fixvalue.TranslateCode;
import android.database.Cursor;

public class DataSentence implements Serializable{
	private int id;
	private String sentence;
	private TranslateCode fromCode, toCode;
	private Calendar mCalendar;
	
	public DataSentence(int id, String sentence, TranslateCode fromCode,
			TranslateCode toCode, Calendar mCalendar) {
		super();
		this.id = id;
		this.sentence = sentence;
		this.fromCode = fromCode;
		this.toCode = toCode;
		this.mCalendar = mCalendar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	public TranslateCode getFromCode() {
		return fromCode;
	}

	public void setFromCode(TranslateCode fromCode) {
		this.fromCode = fromCode;
	}

	public TranslateCode getToCode() {
		return toCode;
	}

	public void setToCode(TranslateCode toCode) {
		this.toCode = toCode;
	}

	public Calendar getmCalendar() {
		return mCalendar;
	}

	public void setmCalendar(Calendar mCalendar) {
		this.mCalendar = mCalendar;
	}
	
	
}
