package lhy.advtranslator;

import java.util.ArrayList;

import com.mocoplex.adlib.AdlibConfig;

import lhy.advtranslator.fixvalue.TranslateCode;
import lhy.advtranslator.function.C;
import lhy.advtranslator.function.Lhy_Function;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements OnClickListener, OnItemSelectedListener{
	private final String TAG = this.getClass().getSimpleName();
	private Context mContext;

	private View viewHideZone;
	private TextView tvExchange;
	private ArrayList<TranslateCode> codeList;;
	private Spinner spOri, spTranslate;
	private SpinnerAA spOriAA, spTransAA;
	private int posOri, posTrans;

	private EditText etInput;
	private TextView tvOri, tvTranslate1, tvTranslate2;
	private TextView tvShowOri, tvShowTranslate1, tvShowTranslate2;
	private ImageButton ibInputErase;
	private Button btnTranslate1, btnTranslate2;

	private String packageName;
	private String adAdlibr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mContext = this;

		viewHideZone = (View) findViewById(R.id.viewHideZone);
		viewHideZone.setOnClickListener(this);
		
		tvExchange = (TextView) findViewById(R.id.tvExchange);
		tvExchange.setOnClickListener(this);
		
		String[] arrayLangName = getResources().getStringArray(R.array.lang_name);
		String[] arrayLangCode = getResources().getStringArray(R.array.lang_code);
		
		codeList = new ArrayList<TranslateCode>();
		for(int i=0; i<arrayLangName.length; i++){
			String curLangName = arrayLangName[i];
			String curLangCode = arrayLangCode[i];
			codeList.add(new TranslateCode(curLangName, curLangCode));
		}
		
		spOri = (Spinner) findViewById(R.id.spOri);
		spTranslate = (Spinner) findViewById(R.id.spTranslate);
		spOriAA = new SpinnerAA(mContext, R.layout.spinner_item, codeList);
		spTransAA = new SpinnerAA(mContext, R.layout.spinner_item, codeList);
		spOri.setAdapter(spOriAA);
		spTranslate.setAdapter(spTransAA);
		spOri.setOnItemSelectedListener(this);
		spTranslate.setOnItemSelectedListener(this);
		spTranslate.setSelection(1);

		etInput = (EditText) findViewById(R.id.etInput);
		ibInputErase = (ImageButton) findViewById(R.id.ibInputErase);
		ibInputErase.setOnClickListener(this);

		tvOri = (TextView) findViewById(R.id.tvOri);
		tvTranslate1 = (TextView) findViewById(R.id.tvTranslate1);
		tvTranslate2 = (TextView) findViewById(R.id.tvTranslate2);
		tvShowOri = (TextView) findViewById(R.id.tvShowOri);
		tvShowTranslate1 = (TextView) findViewById(R.id.tvShowTranslate1);
		tvShowTranslate2 = (TextView) findViewById(R.id.tvShowTranslate2);
		btnTranslate1 = (Button) findViewById(R.id.btnTranslate1);
		btnTranslate2 = (Button) findViewById(R.id.btnTranslate2);
		btnTranslate1.setOnClickListener(this);
		btnTranslate2.setOnClickListener(this);

		packageName = getPackageName();
		adAdlibr = getResources().getString(R.string.ad_adlibr);
		Log.d(TAG, "Adlib ID : " + adAdlibr);
		initAds();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.viewHideZone:
			Lhy_Function.forceHideKeyboard(mContext, etInput);
			break;
		case R.id.tvExchange:
			int curPosOri = posOri;
			int curPosTrans = posTrans;
			spOri.setSelection(curPosTrans);
			spTranslate.setSelection(curPosOri);
			break;
		case R.id.ibInputErase:
			etInput.setText("");
			break;
		case R.id.btnTranslate1:
			if(etInput.getText().toString().equals("")){
				String msgInputText = getResources().getString(R.string.msg_main_input_sentence);
				Toast.makeText(mContext, msgInputText, Toast.LENGTH_SHORT).show();
			} else{
				String translateNormal = getResources().getString(R.string.main_translated_normal);
				String translateThroughJapanese = getResources().getString(R.string.main_translated_through_japanese);
				TranslateCode curOriCode = codeList.get(posOri);
				TranslateCode curTransCode = codeList.get(posTrans);
				String translateString = etInput.getText().toString();
				tvShowOri.setText(curOriCode.getName());
				tvShowTranslate1.setText(curTransCode.getName() + " " + translateNormal);
				tvShowTranslate2.setText(curTransCode.getName() + " " + translateThroughJapanese);
				new TranslateTask(curOriCode, curTransCode, C.METHOD_ORI, translateString, tvOri, tvTranslate1, tvTranslate2, etInput, mContext).execute();
			}
			break;
		case R.id.btnTranslate2:
			// 사용안함
		}
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		switch(parent.getId()){
		case R.id.spOri:
			posOri = position;
			break;
		case R.id.spTranslate:
			posTrans = position;
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	// AndroidManifest.xml에 권한과 activity를 추가하여야 합니다.     
	protected void initAds() {
		// 광고 스케줄링 설정을 위해 아래 내용을 프로그램 실행시 한번만 실행합니다. (처음 실행되는 activity에서 한번만 호출해주세요.)    	
		// 광고 subview 의 패키지 경로를 설정합니다. (실제로 작성된 패키지 경로로 수정해주세요.)

		// 제휴 플랫폼을 연결합니다.
		AdlibConfig.getInstance().bindPlatform("INMOBI", packageName + ".ads.SubAdlibAdViewInmobi");
		// adlibr.com 에서는 제휴 플랫폼에서 발생한 수익의 일부를 reward point로 더 적립해드립니다.
		// 자세한 사항은 http://adlibr.com/rpoint.jsp 를 참조해주세요.

		// 쓰지 않을 광고플랫폼은 삭제해주세요.
		AdlibConfig.getInstance().bindPlatform("ADAM", packageName + ".ads.SubAdlibAdViewAdam");
		//        AdlibConfig.getInstance().bindPlatform("ADMOB","lhy.undernation.ads.SubAdlibAdViewAdmob");
		AdlibConfig.getInstance().bindPlatform("CAULY", packageName + ".ads.SubAdlibAdViewCauly");
		//        AdlibConfig.getInstance().bindPlatform("TAD","lhy.undernation.ads.SubAdlibAdViewTAD");
		//        AdlibConfig.getInstance().bindPlatform("NAVER","lhy.undernation.ads.SubAdlibAdViewNaverAdPost");
		//        AdlibConfig.getInstance().bindPlatform("SHALLWEAD","lhy.undernation.ads.SubAdlibAdViewShallWeAd");
		// 쓰지 않을 플랫폼은 JAR 파일 및 test.adlib.project.ads 경로에서 삭제하면 최종 바이너리 크기를 줄일 수 있습니다.        

		// SMART* dialog 노출 시점 선택시 / setAdlibKey 키가 호출되는 activity 가 시작 activity 이며 해당 activity가 종료되면 app 종료로 인식합니다.
		// adlibr.com 에서 발급받은 api 키를 입력합니다.
		// http://adlibr.com/admin/myapplist.jsp
		AdlibConfig.getInstance().setAdlibKey(adAdlibr);        


		/*
			         // Locale 별 다른 스케줄을 적용하신다면,
			         Locale locale = this.getResources().getConfiguration().locale;
			         String lc = locale.getLanguage();

			         if(lc.equals("ko"))
			         {
			         // 다국어 스케줄을 설정하시려면 애드립에서 별도로 키를 생성하시고 해당 키를 적용해주세요.
			         AdlibConfig.getInstance().setAdlibKey("대한민국 광고 스케줄링");
			         }
			         else
			         {
			         // 다국어 스케줄을 설정하시려면 애드립에서 별도로 키를 생성하시고 해당 키를 적용해주세요.
			         AdlibConfig.getInstance().setAdlibKey("그밖의 나라");
			         }
		 */


		/* 
		 * deprecated : SMART* dialog 를 통해 보다 쉽게 버전관리를 할 수 있습니다.
		 */

		/*
					// 광고뷰가 없는 activity 에서는 listener 대신 아래와 같은 방법으로 설정한 현재 버전을 가져올 수 있습니다.
					// 애드립에서 설정한 버전정보를 아래와 같이 수신합니다.
			        final Handler handler = new Handler();
			        handler.postDelayed(new Runnable() {
			          @Override
			          public void run() {
			       	  	String ver = _amanager.getCurrentVersion();
			          }
			        }, 1000);
		 */        
		/*
			        // 클라이언트 버전관리를 위한 리스너 추가
			        // 서버에서 클라이언트 버전을 관리하여 사용자에게 업데이트를 안내할 수 있습니다. (option)
			        this.setVersionCheckingListner(new AdlibVersionCheckingListener(){

						@Override
						public void gotCurrentVersion(String ver) {

							// 서버에서 설정한 버전정보를 수신했습니다.
							// 기존 클라이언트 버전을 확인하여 적절한 작업을 수행하세요.
							double current = 0.9;

							double newVersion = Double.parseDouble(ver);				
							if(current >= newVersion)
								return;


							new AlertDialog.Builder(AdlibTestProjectActivity.this)
						    .setTitle("버전 업데이트")
						    .setMessage("프로그램을 업데이트 하시겠습니까?")
								    .setPositiveButton("yes", new DialogInterface.OnClickListener() {
								      public void onClick(DialogInterface dialog, int whichButton) {
								    	  // 마켓 또는 안내 페이지로 이동합니다.
								      }
								    })	    
								    .setNegativeButton("no", new DialogInterface.OnClickListener() {
								      public void onClick(DialogInterface dialog, int whichButton) {
								    	  // 업데이트를 하지 않습니다.
								      }
								    })	    
						    .show();

						}
			        });
		 */

	}

	


}
